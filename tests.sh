#!/usr/bin/env bash
echo "TEST:::CET CHECKED DOMAIN google.com : unavailable"
echo "google.com" | ./domainsFromInput.sh
echo "TEST:::CET CHECKED DOMAIN aaaaaaaaaaaaaaaar.be : !!! available !!!"
echo "aaaaaaaaaaaaaaaar.be" | ./domainsFromInput.sh

sleep 2s
./domainsFromInput.sh google.com
./domainsFromInput.sh aaaaaaaaaaaaaaaar.be
sleep 2s
./domainsFromInput.sh testDomains.txt


