# Domain name availability check for Ubuntu

On ubuntu, checks if domains are available, and notifies user by sending a notification (notify-send).
Can be used with cron.

Made for Ubuntu (20.04), other distros may be compatible.

# Setup

Clone this repository:
``` bash
git clone https://gitlab.com/timothee-peron/domain-name-avail.git
```

A line in the crontab could be (edit with crontab -e command):
``` bash
0 * * * *     cat ~/domain-name-avail/testDomains.txt | ~/domain-name-avail/domainsFromInput.sh
```

With output redirection to log:
``` bash
0 * * * *     cat ~/domain-name-avail/testDomains.txt | ~/domain-name-avail/domainsFromInput.sh >> ~/domainsCron.log
```


