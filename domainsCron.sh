#!/usr/bin/env bash
export XDG_RUNTIME_DIR=/run/user/$(id -u) 
 
#List of domains to check, first should succeed, the other not
DOMAINS=( 'google.com' 'iamavvailllllable.be' )

prefix=$(date)
notif_title="Domain available!"
notif_content=""

for domain in "${DOMAINS[@]}"; do
  whois $domain | egrep -q '^No match|^NOT FOUND|^Not fo|AVAILABLE|^No Data Fou|has not been regi|No entri' 
  dispo=$?
  if [ $dispo -eq 0 ]; then
    #Domain is available
    echo "$prefix CHECKED DOMAIN $domain : !!! available !!!" 
    #if not empty, add new line
    [[ -z $notif_content ]] || notif_content+=" "
    notif_content+="$domain"
  else
    #Domain already reserved
    echo "$prefix CHECKED DOMAIN $domain : unavailable"
  fi 
done 

# if not empty, send notif for ubuntu
[[ -z $notif_content ]] || notify-send "$notif_title" "Check $notif_content availability!"
