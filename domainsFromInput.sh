#!/usr/bin/env bash
export XDG_RUNTIME_DIR=/run/user/$(id -u) 

DOMAINS=( )

# This command will try in this order:
# 1- to read from stdin (pipe...)
# 2- to read from the file named as first argument (if any)
# 3- to read the arguments (if any)
# 4- to read one entry from user input
if [ -p /dev/stdin ]; then
  #echo "Data was piped to this script!"
  while IFS= read line; do
    #echo "Line: ${line}"
    DOMAINS=( "${DOMAINS[@]}" ${line})
  done
else
  #echo "No input was found on stdin, skipping!"
  if [ -f "$1" ]; then
    #echo "Filename specified: ${1}"
    readarray -t DOMAINS < ${1}
  else
    if [ -z "$1" ]; then
      #echo "No input was specified!"
      read domain
      DOMAINS=( $domain )
    else
      #echo "Testing these domains: $1 $2..."
      for domain in $@; do
      DOMAINS=( ${DOMAINS[@]} $domain )
      done
    fi
  fi
fi


prefix=$(date)
notif_title="Domain available!"
notif_content=""

for domain in "${DOMAINS[@]}"; do
  whois $domain | egrep -q '^No match|^NOT FOUND|^Not fo|AVAILABLE|^No Data Fou|has not been regi|No entri' 
  dispo=$?
  if [ $dispo -eq 0 ]; then
    #Domain is available
    echo "$prefix CHECKED DOMAIN $domain : !!! available !!!" 
    #if not empty, add new line
    [[ -z $notif_content ]] || notif_content+=" "
    notif_content+="$domain"
  else
    #Domain already reserved
    echo "$prefix CHECKED DOMAIN $domain : unavailable"
  fi 
done 

# if not empty, send notif for ubuntu
[[ -z $notif_content ]] || notify-send "$notif_title" "Check $notif_content availability!"
